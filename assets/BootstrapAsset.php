<?php
namespace app\assets;

class BootstrapAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@bower/bootstrap/dist';
    public $css = [
        'css/bootstrap.css',
    ];
    public $js = [
        'js/bootstrap.min.js'
    ];
}