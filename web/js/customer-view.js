$(document).ready(function(){

    $("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
        e.preventDefault();
        var id = $(e.target).attr("href").substr(1);
        window.location.hash = id;
    });

    var hash = window.location.hash;
    $('#customer-tab a[href="' + hash + '"]').tab('show');

    $('.add-service').click(function(e){
        e.preventDefault();
        let url = $(this).attr('href');
        let modal = $(this).attr('data-target');

        // AJAX request
        $.ajax({
            url: url,
            type: 'get',
            success: function(response){
                // Add response in Modal body
                $('.modal-content').html(response);

                // Display Modal
                $(modal).modal('show');
            }
        });
    });
});
$(document).on('beforeSubmit', 'form.ajax-form', function (e) {
    e.preventDefault();
    let form = $(this);
    let data = new FormData(form[0]);

    $(form).find('div.form-group').removeClass('has-error');

    $.ajax({
        url: form.attr('action'),
        type: 'post',
        data: data,
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
            if (data.reload) {
                window.location.reload();
            }
            if(data.errors){
                $.each(data.errors, function (key, value) {
                    let input = $(form).find('[name$="['+key+']"]');
                    $(input).closest('div.form-group').addClass('has-error').find('.help-block').text(value);
                });
            }
        }
    }).then(r => {
    });
    return false;
});
