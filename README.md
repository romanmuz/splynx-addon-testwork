Splynx Add-on Test Work
======================

Splynx Add-on Skeleton based on [Yii2](http://www.yiiframework.com).

INSTALLATION
------------

Install Splynx Add-on Test Work:
~~~
cd /var/www/splynx/addons/
git clone git@bitbucket.org:romanmuz/splynx-addon-testwork.git
cd splynx-addon-testwork
composer install
./yii install
~~~

Create symlink:
~~~
ln -s /var/www/splynx/addons/splynx-addon-testwork/web/ /var/www/splynx/web/testwork
~~~

Create Nginx config file:
~~~
sudo nano /etc/nginx/sites-available/splynx-testwork.addons
~~~

with following content:
~~~
location /testwork
{
        try_files $uri $uri/ /testwork/index.php?$args;
}
~~~

Restart Nginx:
~~~
sudo service nginx restart
~~~

You can then access Splynx Add-On Skeleton in a menu "Customers".