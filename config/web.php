<?php

return function ($params, $baseDir) {
    return [
        'components' => [
            'request' => [
                'baseUrl' => '/testwork',
                'enableCookieValidation' => false
            ],
            'user' => [
                'identityClass' => 'splynx\models\Admin',
                'idParam' => 'splynx_admin_id',
                'loginUrl' => '/admin/login/?return=%2Ftestwork%2F',
                'enableAutoLogin' => false,
            ],

        ],
    ];
};
