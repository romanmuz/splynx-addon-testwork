<?php


namespace app\models;


use app\models\traits\StatusInfo;
use splynx\v2\models\services\BaseCustomService;
use splynx\v2\models\tariffs\BaseCustomTariff;

/**
 * Class CustomService
 * @package app\models
 */
class CustomService extends BaseCustomService
{
    use StatusInfo;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description', 'tariff_id', 'customer_id', 'quantity'], 'required'],
            [['tariff_id', 'customer_id', 'quantity'], 'integer'],
            [['unit_price'], 'double'],
            [['start_date', 'end_date'], 'date'],
            [['description', 'status'], 'string'],

        ];
    }

    /**
     * @return BaseCustomTariff|null
     * @throws \splynx\base\ApiResponseException
     */
    public function getTariff(){
        return (new BaseCustomTariff())->findById($this->tariff_id);
    }

    /**
     * @return BaseCustomTariff[]|null []|null
     */
    public function getAllTariff(){
        return (new BaseCustomTariff())->findAll([]);
    }

}