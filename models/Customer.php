<?php

namespace app\models;

use app\models\traits\StatusInfo;
use splynx\v2\models\customer\BaseCustomer;
use splynx\v2\models\services\BaseService;
use yii\helpers\ArrayHelper;

/**
 * Class Customer
 * @package app\models
 */
class Customer extends BaseCustomer
{
    use StatusInfo;

    /**
     * @return Customer[]|null
     */
    public function getCustomersList()
    {
        return $this->findAll(
            [],
            [],
            [
                'id' => 'ASC'
            ]
        );
    }

    /**
     * @param $id
     * @return Customer|null
     * @throws \splynx\base\ApiResponseException
     */
    public function getById($id){
        return $this->findOne(['id'=>$id]);
    }

    /**
     * @return InternetService[]|null
     */
    public function getInternetService()
    {
        return $this->getServiceList(new InternetService());
    }

    /**
     * @return VoiceService[]|null
     */
    public function getVoiceService()
    {
        return $this->getServiceList(new VoiceService());
    }

    /**
     * @return CustomService[]|null
     */
    public function getCustomService(): ?array
    {
        return $this->getServiceList(new CustomService());
    }

    /**
     * @param $service BaseService
     * @return mixed
     */
    protected function getServiceList($service){
        $service->customer_id = $this->id;
        $result = ArrayHelper::index($service->findAll([]), 'id');
        return $result;
    }

    /**
     * @return array Get Customer status list
     */
    public function getStatusList()
    {
        return [
            'new' => \Yii::t('app', 'New'),
            'active' => \Yii::t('app', 'Active'),
            'blocked' => \Yii::t('app', 'Blocked'),
            'disabled' => \Yii::t('app', 'Disabled')
        ];
    }


}
