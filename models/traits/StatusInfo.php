<?php
namespace app\models\traits;

use Yii;

/**
 * Trait StatusInfo
 * @package app\models\traits
 */
trait StatusInfo
{
    /**
     * @var string[] List status classes
     */
    public static $classList = [
        'pending' => 'label-primary',
        'active' => 'label-info',
        'hidden' => 'label-default',
        'new' => 'label-primary',
        'blocked' => 'label-danger',
        'disabled' => 'label-default'
    ];


    /**
     * @return string Get current status class
     */
    public function getStatusClass()
    {
        return static::$classList[$this->status] ?? '';
    }

    /**
     * @return array Get List status in dropdown select
     */
    public function getStatusList()
    {
        return [
            'pending' => Yii::t('app', 'Pending'),
            'active' => Yii::t('app', 'Active'),
            'hidden' => Yii::t('app', 'Hidden'),
            'disabled' => Yii::t('app', 'Disabled')
        ];
    }
}