<?php


namespace app\models;


use app\models\traits\StatusInfo;
use splynx\v2\models\services\BaseInternetService;
use splynx\v2\models\tariffs\BaseInternetTariff;

/**
 * Class InternetService
 * @package app\models
 */
class InternetService extends BaseInternetService
{
    use StatusInfo;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description', 'tariff_id', 'customer_id', 'quantity'], 'required'],
            [['tariff_id', 'customer_id', 'quantity'], 'integer'],
            [['unit_price'], 'double'],
            [['start_date', 'end_date'], 'date'],
            [['description', 'status', 'login'], 'string'],

        ];
    }

    /**
     * @return BaseInternetTariff|null
     * @throws \splynx\base\ApiResponseException
     */
    public function getTariff(){
        return (new BaseInternetTariff())->findById($this->tariff_id);
    }

    /**
     * @return BaseInternetTariff[]|null
     */
    public function getAllTariff(){
        return (new BaseInternetTariff())->findAll([]);
    }

}