<?php


namespace app\models;


use app\models\traits\StatusInfo;
use splynx\models\tariffs\BaseVoiceTariff;
use splynx\v2\models\services\BaseVoiceService;

/**
 * Class VoiceService
 * @package app\models
 */
class VoiceService extends BaseVoiceService
{
    use StatusInfo;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description', 'tariff_id', 'customer_id', 'quantity'], 'required'],
            [['tariff_id', 'customer_id', 'quantity'], 'integer'],
            [['unit_price'], 'double'],
            [['start_date', 'end_date'], 'date'],
            [['description', 'status', 'phone'], 'string'],

        ];
    }

    /**
     * @return BaseVoiceTariff|null
     */
    public function getTariff(){
        return (new BaseVoiceTariff())->findById($this->tariff_id);
    }


    /**
     * @return BaseVoiceTariff[]|null
     */
    public function getAllTariff(){
        return (new BaseVoiceTariff())->findAll([]);
    }



}