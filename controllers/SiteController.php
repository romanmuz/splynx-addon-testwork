<?php

namespace app\controllers;

use app\models\Customer;
use app\models\CustomService;
use app\models\InternetService;
use app\models\VoiceService;
use Yii;
use yii\base\InvalidArgumentException;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class SiteController
 * @package app\controllers
 */
class SiteController extends Controller
{

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $this->getView()->title = Yii::t('app', 'Splynx Add-on Customer List');
        $model = new Customer();

        $customerList = $model->getCustomersList();

        return $this->render('index', [
            'customersList' => $customerList
        ]);
    }

    /**
     * @param int $id
     * @return array|string
     * @throws \splynx\base\ApiResponseException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionView($id)
    {
        $this->view->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Splynx Add-on Customer Info')];

        $customer = (new Customer())->getById($id);

        if (Yii::$app->request->isPost) {
            $customer->load(Yii::$app->request->post());

            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($customer);
            }

            if ($customer->save()) {
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Customer info has be updated'));
                $this->refresh();
            } else {
                Yii::$app->getSession()->setFlash('errors', Yii::t('app', 'Save has be failed'));

            }
        }

        $internetService = $customer->getInternetService();
        $voiceService = $customer->getVoiceService();
        $customService = $customer->getCustomService();


        $this->view->registerJsFile('@web/js/customer-view.js');
        return $this->render('view', [
            'customer' => $customer,
            'internetService' => $internetService,
            'voiceService' => $voiceService,
            'customService' => $customService,
        ]);
    }

    /**
     * Use this function only Ajax request
     * @param int $customer
     * @param string $type
     * @return array|string
     * @throws NotFoundHttpException
     * @throws \splynx\base\ApiResponseException
     */
    public function actionAddService($customer, $type){
        if (!Yii::$app->request->isAjax){
            throw new NotFoundHttpException();
        }

        $this->view->title = Yii::t('app', 'Add Service');

        $service = null;
        if (empty($customer) || !is_numeric($customer)){
            throw new InvalidArgumentException();
        }

        $customer = (new Customer())->getById($customer);

        switch ($type){
            case 'internet':
                $service = new InternetService();
                $service->login = $customer->login;
                break;
            case 'voice':
                $service = new VoiceService();
                break;
            case 'custom':
                $service = new CustomService();
                break;
            default:
                throw new InvalidArgumentException();
                break;
        }
        $service->customer_id = $customer->getId();


        if(Yii::$app->request->isPost){
            Yii::$app->response->format = Response::FORMAT_JSON;

            $service->load(Yii::$app->request->post());

            if ($service->save()) {
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Added new service to Customer'));
                return ['reload' => true];
            } else {

                return ['errors' => $service->getErrors()];

            }
        }

        $tariffList = $service->getAllTariff();
        $tariffDorpDown = [];
        foreach ($tariffList as $one){
            $tariffDorpDown[$one['id']] = $one['title'].' - '. number_format($one['price'], 2);
        }

        return $this->renderAjax('service_form', ['service' => $service,'tariffDorpDown' => $tariffDorpDown, 'type' => $type]);
    }

    /**
     * Use this function only Ajax request
     * @param int $id
     * @param string $type
     * @return array|string
     * @throws NotFoundHttpException
     */
    public function actionEditService($id, $type){
        if (!Yii::$app->request->isAjax){
            throw new NotFoundHttpException();
        }

        $this->view->title = Yii::t('app', 'Edit Service');

        $service = null;

        switch ($type){
            case 'internet':
                $service = new InternetService();
                break;
            case 'voice':
                $service = new VoiceService();
                break;
            case 'custom':
                $service = new CustomService();
                break;
            default:
                throw new InvalidArgumentException();
                break;
        }

        $service = $service->findById($id);

        if(Yii::$app->request->isPost){
            Yii::$app->response->format = Response::FORMAT_JSON;

            $service->load(Yii::$app->request->post());

            if ($service->save()) {
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Update Customer service ['.$service->description.']'));
                return ['reload' => true];
            } else {

                return ['errors' => $service->getErrors()];

            }
        }

        $tariffList = $service->getAllTariff();
        $tariffDorpDown = [];
        foreach ($tariffList as $one){
            $tariffDorpDown[$one['id']] = $one['title'].' - '. number_format($one['price'], 2);
        }

        return $this->renderAjax('service_form', ['service' => $service,'tariffDorpDown' => $tariffDorpDown, 'type' => $type]);
    }

    /**
     * @param int $id
     * @param string $type
     * @return Response
     * @throws \yii\base\InvalidConfigException
     */
    public function actionDeleteService($id, $type){
        $service = null;

        switch ($type){
            case 'internet':
                $service = new InternetService();
                break;
            case 'voice':
                $service = new VoiceService();
                break;
            case 'custom':
                $service = new CustomService();
                break;
            default:
                throw new InvalidArgumentException();
                break;
        }

        $service = $service->findById($id);
        $customer = $service->customer_id;

        if($service->delete()){
            Yii::$app->getSession()->setFlash('warning', Yii::t('app', 'Deleted Customer service'));
        }
        return $this->redirect('view?id='.$customer.'#2');
    }
}
